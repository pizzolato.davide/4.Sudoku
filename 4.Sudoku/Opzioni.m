//
//  Opzioni.m
//  4.Sudoku
//
//  Created by DavideMac on 24/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import "Opzioni.h"
#import "Define.h"

@interface Opzioni ()

@end

@implementation Opzioni

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self settaValoriDaVariabiliPermanenti];
    [Define settaSfondo:_img_sfondo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)temaModficato:(id)sender {
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [variabiliPermanenti setInteger:[_segment_Tema selectedSegmentIndex] forKey:@"sfondo"];
    [variabiliPermanenti synchronize];
    [Define settaSfondo:_img_sfondo];
}

- (IBAction)difficoltaModificata:(id)sender {
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [variabiliPermanenti setInteger:(int)[_segment_Difficolta selectedSegmentIndex] forKey:@"difficolta"];
    [variabiliPermanenti synchronize];
}

- (IBAction)cancellaStatistiche:(id)sender {
    [Define cancellaStatistiche];
}

- (void)settaValoriDaVariabiliPermanenti {
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [_segment_Difficolta setSelectedSegmentIndex:[variabiliPermanenti integerForKey:@"difficolta"]];
    [_segment_Tema setSelectedSegmentIndex:[variabiliPermanenti integerForKey:@"sfondo"]];
    [variabiliPermanenti synchronize];
}


@end
