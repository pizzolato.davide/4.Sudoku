//
//  Opzioni.h
//  4.Sudoku
//
//  Created by DavideMac on 24/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Opzioni : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *segment_Tema;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment_Difficolta;
@property (weak, nonatomic) IBOutlet UIImageView *img_sfondo;

- (IBAction)temaModficato:(id)sender;
- (IBAction)difficoltaModificata:(id)sender;
- (IBAction)cancellaStatistiche:(id)sender;


@end
