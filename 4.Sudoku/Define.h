//
//  Define.h
//  4.Sudoku
//
//  Created by DavideMac on 25/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Define : UIViewController

#define DIFFICOLTA_FACILE    ((int) 0) //Costanti utilizzate da tutta l'app
#define DIFFICOLTA_MEDIO     ((int) 1)
#define DIFFICOLTA_DIFFICILE ((int) 2)

#define SFONDO_1   ((int)  0)
#define SFONDO_3   ((int)  1)
#define SFONDO_2   ((int)  2)
#define SFONDO_4   ((int)  3)


#define CASELLA_VUOTA   ((int)  0)
#define CASELLA_PIENA   ((int)  1)
#define NUMERO_INIZIALE ((int) 99)

#define SECONDI ((int) 0)
#define MINUTI  ((int) 1)
#define ORE     ((int) 2)

#define SOMMA_CORRETTA ((int) 1022)

+ (void)settaSfondo:(UIImageView *)img_sfondo;
+ (void)cancellaStatistiche;
@end
