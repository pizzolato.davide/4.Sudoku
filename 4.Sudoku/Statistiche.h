//
//  Statistiche.h
//  4.Sudoku
//
//  Created by DavideMac on 24/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Statistiche : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *txt_numeroPartiteGiocate;
@property (weak, nonatomic) IBOutlet UILabel *txt_numeroPartiteVinte;
@property (weak, nonatomic) IBOutlet UILabel *txt_tempoRecordDifficile;
@property (weak, nonatomic) IBOutlet UILabel *txt_tempoRecordMedio;
@property (weak, nonatomic) IBOutlet UILabel *txt_tempoRecordFacile;
@property (weak, nonatomic) IBOutlet UIImageView *img_sfondo;

- (IBAction)cancellaStatistiche:(id)sender;

@end
