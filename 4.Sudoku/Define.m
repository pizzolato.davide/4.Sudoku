//
//  Define.m
//  4.Sudoku
//
//  Created by Studente on 10/02/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import "Define.h"

@interface Define ()

@end

@implementation Define

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (void)settaSfondo:(UIImageView *)img_sfondo {     //metodi pubblici utilizzati da tutta l'app
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    NSString *Sfondo = @"Sfondo";
    Sfondo = [Sfondo stringByAppendingString:[[variabiliPermanenti objectForKey:@"sfondo"] stringValue]] ;
    [img_sfondo setImage:[UIImage imageNamed:Sfondo]]; 
}

+ (void)cancellaStatistiche {
    NSArray *tempoRecord = @[@[ @"00", @"00", @"00" ],
                             @[ @"00", @"00", @"00" ],
                             @[ @"00", @"00", @"00" ]];
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [variabiliPermanenti setObject:tempoRecord forKey:@"tempoRecord"];
    [variabiliPermanenti setInteger:0 forKey:@"partiteGiocate"];
    [variabiliPermanenti setInteger:0 forKey:@"partiteVinte"];
    [variabiliPermanenti synchronize];
}

@end
