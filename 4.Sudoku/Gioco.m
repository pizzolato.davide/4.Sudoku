//
//  Gioco.m
//  4.Sudoku
//
//  Created by DavideMac on 24/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import "Gioco.h"
#import "Define.h"

@interface Gioco ()

@end

@implementation Gioco


int sudokuCompleto        [9][9];
int sudokuGrigliaGioco    [9][9];
int infoSudokuGrigliaGioco[9][9]; //99 = NUMERO_INIZIALE //0 = CASELLA_VUOTA //1 = CASELLA_PIENA
UIButton *caselle         [9][9];
UIButton *casellaSelezionata = nil;
bool partitaConclusa = true;

- (void)viewDidLoad {
    [super viewDidLoad];
    partitaConclusa = true;
    // Do any additional setup after loading the view.
    [Define settaSfondo:_img_sfondo];
    [self attivaBottoniPartitaSalvata]; //LASCIARE PER ULTIMO
}

-(void)viewWillDisappear:(BOOL)animated {    
    if (!partitaConclusa)
        [self salvaPartita];
    partitaConclusa = false;
    for (int x = 0; x < 9; x++)
    {
        for (int y = 0; y < 9; y++)
        {
            sudokuCompleto        [x][y] = 0;
            sudokuGrigliaGioco    [x][y] = 0;
            infoSudokuGrigliaGioco[x][y] = 0;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)attivaBottoniPartitaSalvata {
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    if([variabiliPermanenti boolForKey:@"presentePartitaSalvata"])
    {
        [_btt_nuovaPartita setHidden:NO];
        [_btt_riprendiPartitaSalvata setHidden:NO];
    }
    else
    {
        [_btt_miArrendo setHidden:NO];
        [_btt_controlla setHidden:NO];
        [self creaSudoku];
    }
}

- (IBAction)cominciaNuovaPartita:(id)sender {
    partitaConclusa = false;
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [variabiliPermanenti setInteger:[variabiliPermanenti integerForKey:@"difficolta"] forKey:@"difficoltaPartita"];
    NSDate *startTime = [NSDate date];
    [variabiliPermanenti setObject:startTime forKey:@"startTime"];
    [variabiliPermanenti synchronize];

    for (int x = 0; x < 9; x++)
    {
        for (int y = 0; y < 9; y++)
        {
            sudokuCompleto        [x][y] = 0;   //azzera le matrici
            sudokuGrigliaGioco    [x][y] = 0;
            infoSudokuGrigliaGioco[x][y] = 0;
            [caselle[x][y] setTitle:@"" forState:UIControlStateNormal]; //azzera i titoli bottoni della griglia
            [caselle[x][y] setBackgroundColor:[UIColor whiteColor]];    //cancella eventuali colori sui bottoni
        }
    }
    [self creaSudoku];
    [_btt_nuovaPartita           setHidden:YES];        //Mostra / nasconde le label e i bottoni
    [_btt_riprendiPartitaSalvata setHidden:YES];
    [_btt_miArrendo              setHidden:NO ];
    [_btt_controlla              setHidden:NO ];
    [_lbl_esatto                 setHidden:YES];
    [_lbl_errato                 setHidden:YES];
    [_lbl_verde                  setHidden:YES];
    [_lbl_rosso                  setHidden:YES];
    [_segment_selectSudoku       setHidden:YES];
    [_btt_nuovaPartitaArreso     setHidden:YES];
    
}

- (IBAction)riprendiPartitaSalvata:(id)sender {
    partitaConclusa = false;
    [self caricaPartita];
    [_btt_nuovaPartita           setHidden:YES];
    [_btt_riprendiPartitaSalvata setHidden:YES];
    [_btt_miArrendo              setHidden:NO ];
    [_btt_controlla              setHidden:NO ];
    [self creaBottoniGrigliaGioco:sudokuGrigliaGioco];
}

- (IBAction)controlla:(id)sender {
    if ([self backtrackIsGrigliaFinita:sudokuGrigliaGioco])
    {   //GRIGLIA COMPLETA
        partitaConclusa = true;
        [_btt_miArrendo          setHidden:YES];
        [_btt_controlla          setHidden:YES];
        for (int x = 0; x < 9; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                [caselle[x][y] setBackgroundColor:[UIColor whiteColor]]; //cancella i colori dati dalla selezione della casella
                [caselle[x][y] setUserInteractionEnabled:NO];            //elimina la possibilità di modifiche
                casellaSelezionata = nil;
                [self azzeraSelezioneNumero];
            }
        }
        if ([self isSudokuCorretto:sudokuGrigliaGioco])
        {   //GIUSTO
            NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
            [variabiliPermanenti setInteger:[variabiliPermanenti integerForKey:@"partiteVinte"] + 1 forKey:@"partiteVinte"];
            [variabiliPermanenti setInteger:[variabiliPermanenti integerForKey:@"partiteGiocate"] + 1 forKey:@"partiteGiocate"];
            NSDate *startTime = [variabiliPermanenti objectForKey:@"startTime"];
            NSTimeInterval tempo = [startTime timeIntervalSinceNow];
            [variabiliPermanenti synchronize];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Vittoria"
                                                            message:@"Complimenti hai Vinto!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

            [_btt_nuovaPartitaArreso setHidden:NO];
            [self verificaRecord:tempo];
        }
        else
        {   //SBAGLIATO
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Perso"
                                                            message:@"Hai Perso! Riprova con un altro sudoku!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [self completaSudoku:nil];
        }
    }
    else
    {
        //GRIGLIA INCOMPLETA
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sudoku non completo"
                                                        message:@"Per poter cliccare controlla devi prima riempire tutte le caselle"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void) verificaRecord:(NSTimeInterval)tempo { //NON FUNZIONA QUINDI è DISATTIVATO
    /*NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    int difficolta = (int)[variabiliPermanenti integerForKey:@"difficoltaPartita"];
    NSArray *tempoRecord = [variabiliPermanenti objectForKey:@"tempoRecord"];
    int record  = (([[[tempoRecord objectAtIndex:difficolta] objectAtIndex:ORE]     intValue] * 60) * 60);
        record +=  ([[[tempoRecord objectAtIndex:difficolta] objectAtIndex:MINUTI]  intValue] * 60);
        record +=   [[[tempoRecord objectAtIndex:difficolta] objectAtIndex:SECONDI] intValue];
    if (tempo < record || record == 0)
    {
        int tempoOre = (tempo / (60 * 60));
        tempo -= tempoOre * 60 * 60;
        int tempoMinuti = (tempo / 60);
        tempo -= tempoMinuti * 60;
        int tempoSecondi = tempo;
        NSMutableArray *nuovoTempoRecord = [NSMutableArray arrayWithObjects:[tempoRecord objectAtIndex:0], [tempoRecord objectAtIndex:1], [tempoRecord objectAtIndex:2], nil];
        NSMutableArray *tempoDaInserire = [[NSMutableArray alloc] initWithCapacity:3];
        [tempoDaInserire insertObject:[NSNumber numberWithInt:tempoOre] atIndex:ORE];
        [tempoDaInserire insertObject:[NSNumber numberWithInt:tempoMinuti] atIndex:MINUTI];
        [tempoDaInserire insertObject:[NSNumber numberWithInt:tempoSecondi] atIndex:SECONDI];
        [nuovoTempoRecord insertObject:[NSArray arrayWithArray:tempoDaInserire] atIndex:difficolta];
        [variabiliPermanenti setObject:[NSArray arrayWithArray:nuovoTempoRecord] forKey:@"tempoRecord"];
    }
    [variabiliPermanenti synchronize];*/
}


- (IBAction)completaSudoku:(id)sender {
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [variabiliPermanenti setInteger:[variabiliPermanenti integerForKey:@"partiteGiocate"] + 1 forKey:@"partiteGiocate"];
    [variabiliPermanenti synchronize];
    partitaConclusa = true;
    if ([self isSudokuCorretto:sudokuGrigliaGioco])
    {//GIUSTO
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Perchè ti arrendi?"
                                                        message:@"HAI VINTO!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [self controlla:nil];
    }
    else
    {//SBAGLIATO QUINDI STAMPA SOLUZIONE CORRETTA
        [_btt_miArrendo          setHidden:YES];
        [_btt_controlla          setHidden:YES];
        [_lbl_esatto             setHidden:NO];
        [_lbl_errato             setHidden:NO];
        [_lbl_verde              setHidden:NO];
        [_lbl_rosso              setHidden:NO];
        [_segment_selectSudoku   setHidden:NO];
        [_btt_nuovaPartitaArreso setHidden:NO];
        for (int x = 0; x < 9; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                [caselle[x][y] setBackgroundColor:[UIColor whiteColor]];
                if (infoSudokuGrigliaGioco[x][y] != NUMERO_INIZIALE)
                {
                    if (sudokuGrigliaGioco[x][y] == sudokuCompleto[x][y])
                    {//NUMERO CORRETTO
                        [caselle[x][y] setBackgroundColor:[UIColor colorWithRed:0.0 green:255.0 blue:0.0 alpha:1.0]];
                    }
                    else
                    {//NUMERO ERRATO
                        [caselle[x][y] setBackgroundColor:[UIColor colorWithRed:255.0 green:0.0 blue:0.0 alpha:1.0]];
                    }
                    [caselle[x][y] setUserInteractionEnabled:NO]; //elimina la possibilita di modifica
                    casellaSelezionata = nil;
                    [self azzeraSelezioneNumero];
                    [caselle[x][y] setTitle:[NSString stringWithFormat:@"%d",  sudokuCompleto[x][y]] forState:UIControlStateNormal];
                }
            }
        }
    }
}

- (IBAction)numeroCliccato:(id)sender {
    UIButton *bottoneCliccato = (UIButton *) sender;
    if (casellaSelezionata != nil)
    {
        [self azzeraSelezioneNumero];//toglie il colore agli altri numeri
        [bottoneCliccato setTitleColor:[UIColor colorWithRed:255.0 green:0.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];//colora il numero cliccato
        int numeroCliccato = (int)[bottoneCliccato    tag];
        int yBottone       = (int)[casellaSelezionata tag] % 10;
        int xBottone       = (int)[casellaSelezionata tag] / 10;
        if (numeroCliccato == CASELLA_VUOTA)
        {
            [casellaSelezionata setTitle:@"" forState:UIControlStateNormal];
            infoSudokuGrigliaGioco[xBottone][yBottone] = CASELLA_VUOTA;
        }
        else
        {
            [casellaSelezionata setTitle:[NSString stringWithFormat:@"%d", numeroCliccato] forState:UIControlStateNormal];
            infoSudokuGrigliaGioco[xBottone][yBottone] = CASELLA_PIENA;
        }
        sudokuGrigliaGioco    [xBottone][yBottone] = numeroCliccato;
    }
}

- (IBAction)segmentSudokuChange:(id)sender { //Stampa soluzione o griglia del giocatore
    if ([_segment_selectSudoku selectedSegmentIndex] == 0)
        [self stampaCaselle:sudokuGrigliaGioco];//griglia del giocatore
    else
        [self stampaCaselle:sudokuCompleto];//soluzione
}

-(void) stampaCaselle:(int[9][9])sudokuDaStampare
{
    for (int x = 0; x < 9; x++)
    {
        for (int y = 0; y < 9; y++)
        {
            if(sudokuDaStampare[x][y] == CASELLA_VUOTA)
                [caselle[x][y] setTitle:@"" forState:UIControlStateNormal];
            else
                [caselle[x][y] setTitle:[NSString stringWithFormat:@"%d",  sudokuDaStampare[x][y]] forState:UIControlStateNormal];
        }
    }
}

-(void) casellaCliccata:(id)sender {//casella del sudoku cliccata
    casellaSelezionata = (UIButton *) sender;
    [self azzeraSelezioneNumero];
    int yBottone = (int)[casellaSelezionata tag] % 10;
    int xBottone = (int)[casellaSelezionata tag] / 10;
    UIColor *coloreRosso = [UIColor colorWithRed:255.0 green:0.0 blue:0.0 alpha:1.0];
    UIButton *bttNumeri[10] = {_btt_casellaVuota, _btt_1, _btt_2, _btt_3, _btt_4, _btt_5, _btt_6, _btt_7, _btt_8, _btt_9};
    [bttNumeri[sudokuGrigliaGioco[xBottone][yBottone]] setTitleColor:coloreRosso forState:UIControlStateNormal];//setta il colore rosso al numero presente su quella casella
    for (int x = 0; x < 9; x++)
    {
        for (int y = 0; y < 9; y++)
        {
            [caselle[x][y] setBackgroundColor:[UIColor whiteColor]];//azzera i colori della griglia
        }
    }
    for (int y = 0; y < 9; y++)
    {
        [caselle[xBottone][y] setBackgroundColor:[UIColor colorWithRed:0.55 green:1.00 blue:0.89 alpha:1.0]];//colora la colonna
    }
    for (int x = 0; x < 9; x++)
    {
        [caselle[x][yBottone] setBackgroundColor:[UIColor colorWithRed:0.55 green:1.00 blue:0.89 alpha:1.0]];//colora la riga
    }
    int xQuadrato = xBottone / 3;
    int yQuadrato = yBottone / 3;
    for (int y = yQuadrato * 3; y < (yQuadrato * 3) + 3; y++)
    {
        for (int x = xQuadrato * 3; x < (xQuadrato * 3) + 3; x++)
        {
            [caselle[x][y] setBackgroundColor:[UIColor colorWithRed:0.55 green:1.00 blue:0.89 alpha:1.0]];//colora il quadrato
        }
    }
    [casellaSelezionata setBackgroundColor:[UIColor colorWithRed:0.55 green:1.00 blue:0 alpha:1.0]];//colora la casella
}

-(void) azzeraSelezioneNumero {
    UIColor *defaultColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    UIButton *bttNumeri[10] = {_btt_casellaVuota, _btt_1, _btt_2, _btt_3, _btt_4, _btt_5, _btt_6, _btt_7, _btt_8, _btt_9};
    for (int i = 0; i < 10; i++)
        [bttNumeri[i]  setTitleColor:defaultColor forState:UIControlStateNormal];//azzera il colore
}

-(void) creaSudoku {
    partitaConclusa = false;
    if ([self backtrack:0 :0])
    {
        NSLog(@"%@", [NSString stringWithFormat:@"%d", [self isSudokuCorretto:sudokuCompleto]]);
        [self creaSudokuNonCompleto];
        [self creaBottoniGrigliaGioco:sudokuGrigliaGioco];
    }
}

- (bool)isSudokuCorretto:(int [9][9]) sudoku {
    for (int x = 0; x < 9; x++)//colonne
    {
        int sommaColonna = 0;
        for (int y = 0; y < 9; y++)
        {
            sommaColonna += pow (2, sudoku[x][y]);
        }
        if (sommaColonna != SOMMA_CORRETTA)
            return false;
    }
    for (int y = 0; y < 9; y++)//righe
    {
        int sommaRiga = 0;
        for (int x = 0; x < 9; x++)
        {
            sommaRiga += pow (2, sudoku[x][y]);
        }
        if (sommaRiga != SOMMA_CORRETTA)
            return false;
    }
    
    for (int yQuadrato = 0; yQuadrato < 3; yQuadrato++) //quadrati
    {
        for (int xQuadrato = 0; xQuadrato < 3; xQuadrato++)
        {
            int sommaQuadrato = 0;
            for (int y = yQuadrato * 3; y < (yQuadrato * 3) + 3; y++)
            {
                for (int x = xQuadrato * 3; x < (xQuadrato * 3) + 3; x++)
                {
                    sommaQuadrato += pow (2, sudoku[x][y]);
                }
            }
            if (sommaQuadrato != SOMMA_CORRETTA)
                return false;
        }
    }
    return true;
}

/*  --------------------------------------
    -----------INIZIO BACKTRACK-----------
    --------------------------------------  */


-(bool) backtrack:(int) xPos :(int) yPos {

    if ([self backtrackIsGrigliaFinita:sudokuCompleto])
    {
            return true;
    }
    
    int nextX, nextY;
    if (xPos == 8)
    {
        nextX = 0;
        nextY = yPos + 1;
    }
    else
    {
        nextX = xPos + 1;
        nextY = yPos;
    }
    if (yPos == 9)
        NSLog(@"Out of limit");
    for (int i = 1; i <= 9; i++)
    {
        sudokuCompleto[xPos][yPos] = i;
        if ([self backtrackIsGrigliaValida :(int) xPos :(int)yPos])
        {
            if ([self backtrack:nextX :nextY])
                return true;
        }
    }
    sudokuCompleto[xPos][yPos] = CASELLA_VUOTA;
    return false;
}

-(bool) backtrackIsGrigliaFinita :(int[9][9]) sudoku {
    for (int y = 0; y < 9; y++)
    {
        for (int x = 0; x < 9; x++)
        {
            if (sudoku[x][y] == CASELLA_VUOTA)
                return false;
        }
    }
    return true;
}

-(bool) backtrackIsGrigliaValida :(int) xVerifica :(int) yVerifica{
    for (int x = 0; x < 9; x++)//riga
    {
        if (x == xVerifica)
            continue;
        if (sudokuCompleto[x][yVerifica] == sudokuCompleto[xVerifica][yVerifica])
            return false;
    }
    for (int y = 0; y < 9; y++)//colonna
    {
        if (y == yVerifica)
            continue;
        if (sudokuCompleto[xVerifica][y] == sudokuCompleto[xVerifica][yVerifica])
            return false;
    }
    int xQuadrato = xVerifica / 3;
    int yQuadrato = yVerifica / 3;
    for (int y = yQuadrato * 3; y < (yQuadrato * 3) + 3; y++)//quadrato
    {
        for (int x = xQuadrato * 3; x < (xQuadrato * 3) + 3; x++)
        {
            if ((y == yVerifica) && (x == xVerifica))
                continue;
            if (sudokuCompleto[x][y] == sudokuCompleto[xVerifica][yVerifica])
                return false;
        }
    }
    return true;
}

/*  --------------------------------------
    ------------FINE BACKTRACK------------
    --------------------------------------  */

- (void)creaSudokuNonCompleto {
    for (int y = 0; y < 9; y++)
    {
        for (int x = 0; x < 9; x++)
        {
            infoSudokuGrigliaGioco[x][y] = NUMERO_INIZIALE;
            sudokuGrigliaGioco[x][y] = sudokuCompleto[x][y];
        }
    }
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    int difficolta = (int)[variabiliPermanenti integerForKey:@"difficolta"];
    [variabiliPermanenti synchronize];
    int numeriDaTogliere = 5;
    switch (difficolta) {
        case DIFFICOLTA_FACILE:
            numeriDaTogliere = 4;
            break;
        case DIFFICOLTA_MEDIO:
            numeriDaTogliere = 5;
            break;
        case DIFFICOLTA_DIFFICILE:
            numeriDaTogliere = 6;
            break;
    }
    int yQuadrato = 0;
    for (int xQuadrato = 0; xQuadrato < 3; xQuadrato++)
    {
        int numeriTolti = 0;
        for (int y = yQuadrato * 3; y < (yQuadrato * 3) + 3; y++)
        {
            for (int x = xQuadrato * 3; x < (xQuadrato * 3) + 3; x++)
            {
                if (((arc4random() % 2) == 0) && (numeriTolti++ < numeriDaTogliere))
                {
                    sudokuGrigliaGioco    [x][y]         = CASELLA_VUOTA;
                    infoSudokuGrigliaGioco[x][y]         = CASELLA_VUOTA;
                    sudokuGrigliaGioco    [8 - x][8 - y] = CASELLA_VUOTA;
                    infoSudokuGrigliaGioco[8 - x][8 - y] = CASELLA_VUOTA;
                }
                else
                {
                    infoSudokuGrigliaGioco[x][y]         = NUMERO_INIZIALE;
                    infoSudokuGrigliaGioco[8 - x][8 - y] = NUMERO_INIZIALE;
                }
            }
        }
    }
    yQuadrato = 1;
    for (int xQuadrato = 0; xQuadrato < 3; xQuadrato+=2)
    {
        int numeriTolti = 0;
        for (int y = yQuadrato * 3; y < (yQuadrato * 3) + 3; y++)
        {
            for (int x = xQuadrato * 3; x < (xQuadrato * 3) + 3; x++)
            {
                if (((arc4random() % 2) == 0) && (numeriTolti++ < numeriDaTogliere))
                {
                    sudokuGrigliaGioco    [x][y]         = CASELLA_VUOTA;
                    infoSudokuGrigliaGioco[x][y]         = CASELLA_VUOTA;
                    sudokuGrigliaGioco    [8 - x][8 - y] = CASELLA_VUOTA;
                    infoSudokuGrigliaGioco[8 - x][8 - y] = CASELLA_VUOTA;
                }
                else
                {
                    infoSudokuGrigliaGioco[x][y]         = NUMERO_INIZIALE;
                    infoSudokuGrigliaGioco[8 - x][8 - y] = NUMERO_INIZIALE;
                }
            }
        }
    }
        yQuadrato = 1;
    int xQuadrato = 1;
    int numeriTolti = 0;
    for (int y = yQuadrato * 3; y < (yQuadrato * 3) + 3; y++)
    {
        for (int x = xQuadrato * 3; x < (xQuadrato * 3) + 3; x++)
        {
            if (((arc4random() % 2) == 0) && (numeriTolti++ < numeriDaTogliere))
            {
                sudokuGrigliaGioco    [x][y]         = CASELLA_VUOTA;
                infoSudokuGrigliaGioco[x][y]         = CASELLA_VUOTA;
                sudokuGrigliaGioco    [8 - x][8 - y] = CASELLA_VUOTA;
                infoSudokuGrigliaGioco[8 - x][8 - y] = CASELLA_VUOTA;
            }
            else
            {
                infoSudokuGrigliaGioco[x][y]         = NUMERO_INIZIALE;
                infoSudokuGrigliaGioco[8 - x][8 - y] = NUMERO_INIZIALE;
            }
        }
    }
    
}

- (void)creaBottoniGrigliaGioco :(int [9][9]) sudokuDaStampare {
    const float widthCasella    =  35.0;
    const float heightCasella   =  35.0;
    const float distanzaCaselle =   2.3;
    const int   xIniziale       =  23;
    const int   yIniziale       = 167;
    for (int righe = 0; righe < 9; righe++) // Y
    {
        for (int colonne = 0; colonne < 9; colonne++) // X
        {
            float x = (widthCasella  + distanzaCaselle) * colonne + xIniziale;
            float y = (heightCasella + distanzaCaselle) * righe   + yIniziale;
            NSString *numeroCasella = [NSString stringWithFormat:@"%d", sudokuDaStampare[colonne][righe]];
            UIButton *button        = [UIButton buttonWithType:UIButtonTypeSystem];
            caselle[colonne][righe] = button;
            [button setTag:(colonne*10+righe)];
            if (sudokuDaStampare[colonne][righe] == CASELLA_VUOTA)
            {
                [button setTitle:@"" forState:UIControlStateNormal];
                [button addTarget:self
                action:@selector(casellaCliccata:)
                forControlEvents:UIControlEventTouchUpInside];
                infoSudokuGrigliaGioco[colonne][righe] = CASELLA_VUOTA;
            }
            else
            {
                switch (infoSudokuGrigliaGioco[colonne][righe]) {
                    case 0: //partita nuova quindi NUMERO_INIZIALE
                        infoSudokuGrigliaGioco[colonne][righe] = NUMERO_INIZIALE;
                        [button setUserInteractionEnabled:NO];
                        [button setTintColor:[UIColor blackColor]];
                        break;
                    case 1: //partita salvata CASELLA_PIENA
                        [button addTarget:self
                                   action:@selector(casellaCliccata:)
                         forControlEvents:UIControlEventTouchUpInside];
                        break;
                    case 99: //partita salvata NUMERO_INIZIALE
                        [button setUserInteractionEnabled:NO];
                        [button setTintColor:[UIColor blackColor]];
                        break;
                }
                [button setTitle:numeroCasella forState:UIControlStateNormal];
            }
            
            button.frame = CGRectMake(x, y, widthCasella, heightCasella);
            [self.view addSubview:button];
        }
    }
    [self stampaSoluzioneConsole];
}

-(void)stampaSoluzioneConsole {
    for (int y = 0; y < 9; y++)
    {
        NSString *riga = @"";
        for (int x = 0; x < 9; x++)
        {
            riga = [riga stringByAppendingFormat:@"%d - ", sudokuCompleto[x][y]];
        }
        NSLog(@"%@", riga);
    }
}

-(void)salvaPartita {
    NSMutableArray *sudokuCompletoSalvataggio         = [[NSMutableArray alloc] initWithCapacity:9];
    NSMutableArray *sudokuGrigliaGiocoSalvataggio     = [[NSMutableArray alloc] initWithCapacity:9];
    NSMutableArray *infoSudokuGrigliaGiocoSalvataggio = [[NSMutableArray alloc] initWithCapacity:9];
    NSMutableArray *sudokuCompletoSalvataggioColonna        [9];
    NSMutableArray *sudokuGrigliaGiocoSalvataggioColonna    [9];
    NSMutableArray *infoSudokuGrigliaGiocoSalvataggioColonna[9];
    for (int i = 0; i < 9; i++)
    {
        sudokuCompletoSalvataggioColonna        [i] = [[NSMutableArray alloc] initWithCapacity:9];
        sudokuGrigliaGiocoSalvataggioColonna    [i] = [[NSMutableArray alloc] initWithCapacity:9];
        infoSudokuGrigliaGiocoSalvataggioColonna[i] = [[NSMutableArray alloc] initWithCapacity:9];
        
        for (int j = 0; j < 9; j++)
        {
            [sudokuCompletoSalvataggioColonna        [i] insertObject:[NSNumber numberWithInt:sudokuCompleto        [i][j]] atIndex:j];
            [sudokuGrigliaGiocoSalvataggioColonna    [i] insertObject:[NSNumber numberWithInt:sudokuGrigliaGioco    [i][j]] atIndex:j];
            [infoSudokuGrigliaGiocoSalvataggioColonna[i] insertObject:[NSNumber numberWithInt:infoSudokuGrigliaGioco[i][j]] atIndex:j];
        }
        [sudokuCompletoSalvataggio         insertObject:sudokuCompletoSalvataggioColonna        [i] atIndex:i];
        [sudokuGrigliaGiocoSalvataggio     insertObject:sudokuGrigliaGiocoSalvataggioColonna    [i] atIndex:i];
        [infoSudokuGrigliaGiocoSalvataggio insertObject:infoSudokuGrigliaGiocoSalvataggioColonna[i] atIndex:i];
    }
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [variabiliPermanenti setBool  :YES                               forKey:@"presentePartitaSalvata"           ];
    [variabiliPermanenti setObject:sudokuCompletoSalvataggio         forKey:@"sudokuCompletoSalvataggio"        ];
    [variabiliPermanenti setObject:sudokuGrigliaGiocoSalvataggio     forKey:@"sudokuGrigliaGiocoSalvataggio"    ];
    [variabiliPermanenti setObject:infoSudokuGrigliaGiocoSalvataggio forKey:@"infoSudokuGrigliaGiocoSalvataggio"];
    [variabiliPermanenti synchronize];
}

-(void)caricaPartita {
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    [variabiliPermanenti setBool:NO forKey:@"presentePartitaSalvata"];
    NSMutableArray *sudokuCompletoSalvataggio         = [NSMutableArray arrayWithArray:[variabiliPermanenti objectForKey:@"sudokuCompletoSalvataggio"        ]];
    NSMutableArray *sudokuGrigliaGiocoSalvataggio     = [NSMutableArray arrayWithArray:[variabiliPermanenti objectForKey:@"sudokuGrigliaGiocoSalvataggio"    ]];
    NSMutableArray *infoSudokuGrigliaGiocoSalvataggio = [NSMutableArray arrayWithArray:[variabiliPermanenti objectForKey:@"infoSudokuGrigliaGiocoSalvataggio"]];
    [variabiliPermanenti synchronize];
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            sudokuCompleto[i][j]         = [[[sudokuCompletoSalvataggio         objectAtIndex:i] objectAtIndex:j] intValue];
            sudokuGrigliaGioco[i][j]     = [[[sudokuGrigliaGiocoSalvataggio     objectAtIndex:i] objectAtIndex:j] intValue];
            infoSudokuGrigliaGioco[i][j] = [[[infoSudokuGrigliaGiocoSalvataggio objectAtIndex:i] objectAtIndex:j] intValue];
        }
        
    }
}

@end
