//
//  Statistiche.m
//  4.Sudoku
//
//  Created by DavideMac on 24/01/17.
//  Copyright © 2017 Davide Pizzolato. All rights reserved.
//

#import "Statistiche.h"
#import "Define.h"

@interface Statistiche ()

@end

@implementation Statistiche

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self stampaStatistiche];
    [Define settaSfondo:_img_sfondo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction) cancellaStatistiche:(id)sender {
    [Define cancellaStatistiche];
    [self stampaStatistiche];
}

- (void) stampaStatistiche {
    NSUserDefaults *variabiliPermanenti = [NSUserDefaults standardUserDefaults];
    NSArray *tempoRecord = [variabiliPermanenti objectForKey:@"tempoRecord"];
    [variabiliPermanenti synchronize];
    NSString *tempoFacile    = [NSString stringWithFormat:@"%@:%@:%@", [[tempoRecord objectAtIndex:DIFFICOLTA_FACILE]    objectAtIndex:ORE    ],
                                                                       [[tempoRecord objectAtIndex:DIFFICOLTA_FACILE]    objectAtIndex:MINUTI ],
                                                                       [[tempoRecord objectAtIndex:DIFFICOLTA_FACILE]    objectAtIndex:SECONDI]];
    NSString *tempoMedio     = [NSString stringWithFormat:@"%@:%@:%@", [[tempoRecord objectAtIndex:DIFFICOLTA_MEDIO]     objectAtIndex:ORE    ],
                                                                       [[tempoRecord objectAtIndex:DIFFICOLTA_MEDIO]     objectAtIndex:MINUTI ],
                                                                       [[tempoRecord objectAtIndex:DIFFICOLTA_MEDIO]     objectAtIndex:SECONDI]];
    NSString *tempoDifficile = [NSString stringWithFormat:@"%@:%@:%@", [[tempoRecord objectAtIndex:DIFFICOLTA_DIFFICILE] objectAtIndex:ORE    ],
                                                                       [[tempoRecord objectAtIndex:DIFFICOLTA_DIFFICILE] objectAtIndex:MINUTI ],
                                                                       [[tempoRecord objectAtIndex:DIFFICOLTA_DIFFICILE] objectAtIndex:SECONDI]];
    int partiteGiocate = (int)[variabiliPermanenti integerForKey:@"partiteGiocate"];
    int partiteVinte   = (int)[variabiliPermanenti integerForKey:@"partiteVinte"];
    [_txt_tempoRecordFacile    setText:tempoFacile   ];
    [_txt_tempoRecordMedio     setText:tempoMedio    ];
    [_txt_tempoRecordDifficile setText:tempoDifficile];
    [_txt_numeroPartiteGiocate setText:[NSString stringWithFormat:@"%d", partiteGiocate]];
    [_txt_numeroPartiteVinte   setText:[NSString stringWithFormat:@"%d", partiteVinte]];
    
}
@end
